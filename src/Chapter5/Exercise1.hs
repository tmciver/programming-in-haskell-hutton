module Chapter5.Exercise1 where

sumOf100Squared :: Int
sumOf100Squared = sum [x*x | x <- [1..100]]
