module Chapter5.Exercise4 where

-- Real answer:
-- replicate n x = [x | _ <- [1..n]]
replicate :: Int -> a -> [a]
replicate n x = [y | y <- take n xs]
  where xs = x : xs
