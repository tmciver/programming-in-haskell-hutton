module Chapter5.Exercise3 where

import Chapter5.Exercise2

square :: Int -> Grid
square n = [(x, y) | (x, y) <- grid n n, x /= y]
