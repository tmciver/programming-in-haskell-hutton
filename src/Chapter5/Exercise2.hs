module Chapter5.Exercise2 where

type Grid = [(Int, Int)]

grid :: Int -> Int -> Grid
grid m n = [(x, y) | x <- [0..m], y <- [0..n]]
