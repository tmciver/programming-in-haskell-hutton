module Chapter5.Exercise10 where

import Data.Char (ord, chr, isLower, isUpper)

letter2int :: Char -> Int
letter2int c = ord c - ord 'A'

int2letter :: Int -> Char
int2letter i = chr (ord 'A' + i)

shift :: Int -> Char -> Char
shift n c | isLower c || isUpper c = int2letter ((letter2int c + n) `mod` 52)
          | otherwise = c

encode :: Int -> String -> String
encode n s = [shift n c | c <- s]

decode :: Int -> String -> String
--decode n s = [shift (-n) c | c <- s]
decode n s = encode (-n) s
