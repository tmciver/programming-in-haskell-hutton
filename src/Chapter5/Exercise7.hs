module Chapter5.Exercise7 where

foo :: [(Int, Int)]
foo = concat [[(x, y) | x <- [1, 2]] | y <- [3, 4]]
