module Chapter7.Exercise4 where

dec2int :: [Int] -> Int
dec2int = foldl f 0
  where f v i = 10*v + i
