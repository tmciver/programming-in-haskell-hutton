module Chapter7.Exercise3 where

import Prelude hiding (map, filter)

map :: (a -> b) -> [a] -> [b]
map f = foldr g []
  where g x a = f x : a

filter :: (a -> Bool) -> [a] -> [a]
filter p = foldr g []
  where g x a = if p x then x:a else a
