{-# LANGUAGE ScopedTypeVariables #-}

module Chapter7.Exercise2 where

import Prelude hiding (all, any, takeWhile, dropWhile)

all :: forall a. (a -> Bool) -> [a] -> Bool
all p = foldl f True
  where f :: Bool -> a -> Bool
        f b x = b && p x

any :: forall a. (a -> Bool) -> [a] -> Bool
any p = foldl f False
  where f :: Bool -> a -> Bool
        f b x = b || p x

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ [] = []
takeWhile p (x:xs) = if p x
  then x : takeWhile p xs
  else []

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ [] = []
dropWhile p (x:xs) = if p x then dropWhile p xs else x:xs
