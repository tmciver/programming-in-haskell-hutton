module Chapter10.Life where

import Control.Concurrent (threadDelay)
import Control.Monad (forM_)
import System.IO

cls :: IO ()
cls = putStr "\ESC[2J"

type Pos = (Int, Int)

writeAt :: Pos -> String -> IO ()
writeAt p s = goto p >> putStr s

goto :: Pos -> IO ()
goto (x, y) = putStr ("\ESC[" ++ show y ++ ";" ++ show x ++ "H")

width :: Int
width = 10

height :: Int
height = 10

type Board = [Pos]

glider :: Board
glider = [(4, 2), (2, 3), (4, 3), (3, 4), (4, 4)]

showCells :: Board -> IO ()
showCells b = sequence_ [writeAt p "0" | p <- b]

showFrame :: Board -> IO ()
showFrame b = cls >> showCells b >> threadDelay 100000

isAlive :: Board -> Pos -> Bool
isAlive b p = p `elem` b

isEmpty :: Board -> Pos -> Bool
isEmpty b p = not (isAlive b p)

neighbors :: Pos -> [Pos]
neighbors (x, y) = map wrap [ (x-1, y-1), (x-1, y)
                            , (x-1, y+1), (x, y+1)
                            , (x+1, y+1), (x+1, y)
                            , (x+1, y-1), (x, y-1)
                            ]

wrap :: Pos -> Pos
wrap (x, y) = (((x-1) `mod` width) + 1, ((y-1) `mod` height) + 1)

liveNeighbors :: Board -> Pos -> Int
liveNeighbors b = length . filter (isAlive b) . neighbors

survivors :: Board -> [Pos]
survivors b = [p | p <- b, liveNeighbors b p `elem` [2, 3]]

births :: Board -> [Pos]
births b = [(x, y) | x <- [1..width]
                   , y <- [1..height]
                   , isEmpty b (x, y)
                   , liveNeighbors b (x, y) == 3]

rmDups :: Eq a => [a] -> [a]
rmDups [] = []
rmDups (x:xs) = x : rmDups (filter (/= x) xs)

nextGen :: Board -> Board
nextGen b = survivors b ++ births b

-- |Given an initial state produces an infinite list of all future states.
generations :: Board -> [Board]
generations b = iterate nextGen b

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  forM_ (generations glider) showFrame
