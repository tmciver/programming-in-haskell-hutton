module Chapter6.Exercise9 where

import Prelude hiding (sum, take, last)

sum :: Num a => [a] -> a
sum [] = 0
sum (x:xs) = x + sum xs

take :: Int -> [a] -> [a]
take 0 _ = []
take n (x:xs) = x : take (n-1) xs
take _ xs = xs

last :: [a] -> a
last (x:[]) = x
last (_:xs) = last xs
last [] = error "Cannot get last element of an empty list."
