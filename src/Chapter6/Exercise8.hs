module Chapter6.Exercise8 where

import Chapter6.Exercise7 (merge)

halve :: [a] -> ([a], [a])
halve xs = (take m xs, drop m xs)
  where m = length xs `div` 2

msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs' = merge (msort xs) (msort ys)
  where (xs, ys) = halve xs'
