module Chapter6.Exercise2 where

sumdown :: Int -> Int
sumdown 0 = 0
sumdown n | n > 0 = n + sumdown (n-1)
sumdown _ = error "sumdown cannot accept a negative argument."
