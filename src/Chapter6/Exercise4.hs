module Chapter6.Exercise4 where

import Data.Map

euclid :: Int -> Int -> Int
euclid a 0 = a
euclid a b = euclid b (a `mod` b)

-- euclid 6 6 = euclid 6 (6 `mod` 6)
--            = euclid 6 0
--            = 6

-- euclid 6 27 = euclid 6 (6 `mod` 27)
--             = euclid 27 6
--             = euclid 6 (27 `mod` 6)
--             = euclid 6 3
--             = euclid 3 (6 `mod` 3)
--             = euclid 3 0
--             = 3


-- 6 / 27 == 0 remainder 6


-- Moved the countStairs problem to my random-haskell project.

-- countStairs :: Int -> Int
-- countStairs 0 = 1
-- countStairs 1 = 1
-- countStairs n = (countStairs (n-1)) + (countStairs (n-2))

-- countStairs :: Map Int Int -> Int -> Int
-- countStairs _ 0 = 1
-- countStairs _ 1 = 1
-- -- countStairs m steps = let maybeVal = lookup steps m
-- --   in
-- --   if isJust maybeVal
-- --   then let ans = fromJust maybeVal


--   (countStairs m (steps-1)) + (countStairs m (steps-2))
