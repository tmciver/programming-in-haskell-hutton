module Chapter6.Exercise3 where

import Prelude hiding ((^))

infixr 8 ^
(^) :: Int -> Int -> Int
_ ^ 0 = 1
m ^ n = m * m ^ (n-1)

-- 2 ^ 3 ^ 2 == 2 ^ (3 ^ 2)
--              2 ^ 9
--           != (2 ^ 3) ^ 2
--           != 8 ^ 2


-- 2 ^ 3 = 2 * 2 ^ 2
--       = 2 * 2 * 2 ^ 1
--       = 2 * 2 * 2 * 2 ^ 0
--       = 2 * 2 * 2 * 1
--       = 2 * 2 * 2
--       = 2 * 4
--       = 8

-- 2 ^ 1 = 2 * 2 ^ 0
--       = 2 * 1
