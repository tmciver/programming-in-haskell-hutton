module Chapter6.Exercise1 where

fac :: Int -> Int
fac 0 = 1
fac n | n > 0 = n * fac (n - 1)
fac _ = error "fac cannot accept a negative argument."
