module Chapter1.Exercise3 where

import Data.Monoid (Product(..))
import Data.Foldable (fold)

product :: [Int] -> Int
product = foldl (*) 1


product' = getProduct . fold . map Product
