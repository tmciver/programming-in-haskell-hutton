module Chapter4.Exercise3 where

safeTailA :: [a] -> [a]
safeTailA xs = if null xs
  then []
  else tail xs

safeTailB :: [a] -> [a]
safeTailB xs | null xs = []
safeTailB xs = tail xs

safeTailC :: [a] -> [a]
safeTailC [] = []
safeTailC xs = tail xs
