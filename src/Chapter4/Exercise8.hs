module Chapter4.Exercise8 where

luhnDouble :: Int -> Int
luhnDouble i = let j = 2 * i
  in if j > 9
     then j - 9
     else j

luhn :: Int -> Int -> Int -> Int -> Bool
luhn w x y z = let w' = luhnDouble w
                   y' = luhnDouble y
                   s = sum [w', x, y', z]
               in
                 s `mod` 10 == 0
