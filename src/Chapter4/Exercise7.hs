module Chapter4.Exercise7 where

-- mult x y z = x * y * z

mult :: Int -> Int -> Int -> Int
mult x = \y ->
           \z -> x*y*z
