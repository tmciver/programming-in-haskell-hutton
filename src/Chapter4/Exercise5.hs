module Chapter4.Exercise5 where

-- True && True = True
-- _    && _    = False

and :: Bool -> Bool -> Bool
and x y = if x
          then if y
               then True
               else False
          else False
