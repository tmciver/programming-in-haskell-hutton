module Chapter4.Exercise2 where

third :: [a] -> a
third = head . tail . tail

third' :: [a] -> a
third' xs = xs !! 2

third'' :: [a] -> a
third'' (_:_:x:_) = x
third'' _ = error "List must contain at least 3 elements."
