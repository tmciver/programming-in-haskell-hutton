module Chapter4.Exercise6 where

-- True  && b = True
-- False && _    = False

and :: Bool -> Bool -> Bool
and x y = if x
          then y
          else False
