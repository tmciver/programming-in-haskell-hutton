{-# LANGUAGE ScopedTypeVariables #-}

module Chapter8.Exercise3 where

data Tree a = Leaf a
            | Node (Tree a) (Tree a)

t :: Tree Int
t = Node (Node (Leaf 1) (Leaf 4))
         (Node (Leaf 6) (Leaf 9))

tUnbalanced :: Tree Int
tUnbalanced = Node (Node (Node (Leaf 1) (Leaf 2)) (Node (Leaf 4) (Leaf 5)))
                   (Node (Leaf 6) (Leaf 9))

foldTree :: (b -> a -> b) -> b -> Tree a -> b
foldTree f x (Leaf y) = f x y
foldTree f x (Node l r) = let lb = foldTree f x l
  in foldTree f lb r

numLeaves :: forall a. Tree a -> Int
numLeaves = foldTree f 0
  where f :: Int -> a -> Int
        f leaves _ = leaves + 1

balanced :: Tree a -> Bool
balanced (Leaf _) = True
balanced (Node l r) = abs (numLeaves l - numLeaves r) <= 1
  && balanced l && balanced r
