module Chapter8.Exercise4 where

data Tree a = Leaf a
            | Node (Tree a) (Tree a)
            deriving Show

balance :: [a] -> Tree a
balance [] = error "Cannot create Tree from empty list."
balance [x] = Leaf x
balance xs = Node (balance xsl) (balance xsr)
  where (xsl, xsr) = splitAt (div (length xs) 2) xs
