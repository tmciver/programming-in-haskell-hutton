module Chapter8.Exercise2 where

data Tree a = Leaf a
            | Node (Tree a) a (Tree a)

t :: Tree Int
t = Node (Node (Leaf 1) 3 (Leaf 4))
         5
         (Node (Leaf 6) 7 (Leaf 9))

-- This version of 'occurs' is more efficient than the one given in the book
-- because only one comparison is made per node checked whereas the book version
-- can have up to two comparisons (assuming that 'compare' does not do multiple
-- comparisons).
occurs :: Ord a => a -> Tree a -> Bool
occurs x (Leaf y) = x == y
occurs x (Node l y r) = case compare x y of
  EQ -> True
  LT -> occurs x l
  GT -> occurs x r
